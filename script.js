const pagina = document.querySelector("body");
const form = document.querySelector("form");
const links = document.querySelectorAll("a");
const btnLogin = document.querySelectorAll("#login");
const inputEmail = document.querySelector("#email");
const inputSenha = document.querySelector("#senha");

let cor = "blue";

function validarEmail() {
    let email = inputEmail.value;
    let arroba = email.indexOf("@");
    let ultimoArroba = email.lastIndexOf("@");
    let ultimoPonto = email.lastIndexOf(".");

    if (arroba !== ultimoArroba || arroba === -1 || ultimoPonto - arroba < 3) {
        return false;
    }
    return true;
}

function eventoEmail() {
    if (validarEmail()) {
        document.querySelector("#label-email").style.display = "none";
    } else {
        document.querySelector("#label-email").style.display = "block";
    }
}

function validarSenha() {
    let senha = inputSenha.value;
    
    if (!senha.match("[a-zA-Z]") || !senha.match("[0-9]") || senha.length < 6) {
        return false;
    }
    return true;
}

function eventoSenha() {
    if (!validarSenha()) {
        document.querySelector("#label-senha").style.display = "block";
    } else {
        document.querySelector("#label-senha").style.display = "none";        
    }
}

function login() {
    validarEmail();
}

function linksAzuis() {
    for (let link of links) {
        link.style.color = cor;
    }
    cor = cor === "blue" ? "white" : "blue";
}

function crescer() {
    this.style.padding = "60px 60px";
}

function diminuir() {
    this.style.padding = "30px 30px";
}

function girar() {
    this.style.transform = "rotate(180deg)";
}

function desgirar() {
    this.style.transform = "rotate(0deg)";
}

inputEmail.onblur = eventoEmail;
inputSenha.onblur = eventoSenha;


// timeouts e intervals

function chatear() {
    alert("Faça logo seu login");
}
setInterval(chatear, 5000);
//pagina.onclick = eventoEmail;
//form.onmouseenter = crescer;
//form.onmouseleave = diminuir;

/* for (let link of links) {
    link.onmouseenter = girar;
    link.onmouseleave = desgirar;
} */
